package com.wyh.areapic.repository;

import com.wyh.areapic.entity.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 节点接口
 */
@Repository
public interface PersonRepository extends Neo4jRepository<Person, Long> {

    @Query("MATCH (n:Person) WHERE id(n) = $0 RETURN n")
    List<Person> findChildList(Long id);
}

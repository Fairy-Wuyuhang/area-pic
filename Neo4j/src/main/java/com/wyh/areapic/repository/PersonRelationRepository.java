package com.wyh.areapic.repository;

import com.wyh.areapic.entity.PersonRelation;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 * 关系接口
 */
@Repository
public interface PersonRelationRepository extends Neo4jRepository<PersonRelation, Long> {

}

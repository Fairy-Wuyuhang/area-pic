package com.wyh.areapic.controller;

import com.wyh.areapic.config.R;
import com.wyh.areapic.service.impl.AreaServiceImpl;
import com.wyh.areapic.service.impl.PictureServiceImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Map;

@Api(description = "根据图片实时定位接口")
@RestController
@RequestMapping("/picture")
public class PictureController {

    @Autowired
    private PictureServiceImpl pictureService;
    @Autowired
    private AreaServiceImpl areaService;

    @ApiOperation("获取经纬度信息")
    @RequestMapping("open")
    public R getPictureMessage(@RequestParam("file") MultipartFile file) throws IOException {
        File f = new File("C:\\Users\\Administrator\\Pictures\\Saved Pictures\\test.jpg");
        //首先转换一下file
        FileUtils.copyInputStreamToFile(file.getInputStream(),f);
       //1.图片为空
        if(!f.exists()){
            throw new FileNotFoundException("文件为空");
        }
        //2根据图片定位获取拍摄时间与经纬度
        Map<String, Object> map = pictureService.readPic(f);
        String x = (String) map.get("x"); //得到纬度
        String y = (String) map.get("y"); //得到经度

        //3.根据经纬度得到当前具体位置
        Map<String, Object> deptByLocation = areaService.getDeptByLocation(y, x);
        map.put("position",deptByLocation);

        return R.ok().data(map).message("成功");
    }


}

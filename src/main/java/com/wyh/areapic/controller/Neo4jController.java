package com.wyh.areapic.controller;

import com.wyh.areapic.entity.Person;
import com.wyh.areapic.repository.PersonRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.neo4j.driver.Driver;
import org.neo4j.driver.Record;
import org.neo4j.driver.Result;
import org.neo4j.driver.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

import static org.neo4j.driver.Values.parameters;


@RestController
@RequestMapping("/neo4j")
@Api(description = "Neo4j知识图谱测试")
public class Neo4jController {


    @Autowired
    PersonRepository repository;


    @Resource
    Driver driver;

    @ApiOperation("根据id获取子节点")
    @GetMapping("/findChildList")
    public List<Person> findChildList(Long id){
        System.out.println("id : "+id);
        List<Person> childList = repository.findChildList(id);
        for (Person person : childList) {
            System.out.println("person = " + person.toString());
        }
        return childList;
    }

    @ApiOperation("创建节点")
    @GetMapping("/createPerson")
    public String createPerson(){
        Session session = driver.session();

        session.run("CREATE (n:Person {name:$name, age:$age})",
                parameters("name","妮妮同学","age","20"));
        session.run("CREATE (n:Person {name:$name, age:$age})",
                parameters("name","想想同学","age","24"));
        session.run("CREATE (n:Person {name:$name, age:$age})",
                parameters("name","叙同学","age","18"));


        Result result = session.run( "MATCH (n:Person) WHERE n.name = $name " +
                        "RETURN n.name as name, n.age as age",
                parameters( "name", "妮妮同学"));
        while (result.hasNext()) {
            Record record = result.next();
            System.out.println(record.get("name") +":"+ record.get("age"));
        }
        session.close();
        return "ok";
    }

}

package com.wyh.areapic.config;

import org.neo4j.driver.AuthTokens;
import org.neo4j.driver.Driver;
import org.neo4j.driver.GraphDatabase;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Neo4jDriver {


    @Bean
    public Driver driver(){
        return GraphDatabase.driver("bolt://82.157.198.247:7687", AuthTokens.basic("neo4j","123456"));
    }
}
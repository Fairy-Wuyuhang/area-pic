package com.wyh.areapic.service.impl;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Service
public class AreaServiceImpl {

    private final String AK="eWSTKxoTAYvr2PteXpESewtEkzwdFjj2";
    private HashMap<String,Object> map=new HashMap<>();
    /**
     * 1.根据经纬度获取位置信息
     * @param lng： 纬度
     * @param lat： 经度
     * @return
     */
    public Map<String,Object> getDeptByLocation(String lng,String lat){
        if (lng == null || "".equals(lng) || lat == null || "".equals(lat)){
            return null;
        }
        return getDeptByLocation(lng, lat, AK);
    }

    public Map<String,Object> getDeptByLocation(String lng, String lat, String ak) {
        final String URL = "http://api.map.baidu.com/reverse_geocoding/v3/" +
                "?ak=" + ak +
                "&output=json&coordtype=wgs84ll&extensions_town=true&" +
                "location=" + lat + "," + lng;
        String areaCode = "";
        String addressName = "";
        //调百度地图API，通过经纬度查areaCode
        java.net.URL myURL = null;
        URLConnection httpsConn = null;
        //进行转码
        try {
            myURL = new URL(URL);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        try {
            //连接调用api
            httpsConn = (URLConnection) myURL.openConnection();
            if (httpsConn != null) {
                InputStreamReader insr = new InputStreamReader(
                        httpsConn.getInputStream(), "UTF-8");
                BufferedReader br = new BufferedReader(insr);
                String data = null;
                if ((data = br.readLine()) != null) {
                    JSONObject jsonObject = JSON.parseObject(data);
                    //状态码
                    String status = jsonObject.get("status")+"";

                    //其他错误
                    if (!"0".equals(status)) {
                        log.error("百度地图api调用失败: "+lat+","+lng);
                        return null;
                    }
                    //使用alibaba的JSON工具类取自己想要的内容
                    String address=jsonObject.getJSONObject("result").get("formatted_address")+"";
                    String district=jsonObject.getJSONObject("result").getJSONObject("addressComponent").get("district") + "";
                    areaCode = jsonObject.getJSONObject("result").getJSONObject("addressComponent").get("town_code") + "";
                    addressName = jsonObject.getJSONObject("result").getJSONObject("addressComponent").get("town") + "";
                    //区域编码
                    System.out.println("附近地方："+address);
                    System.out.println("所属区域："+district);
                    System.out.println("所属地方code："+areaCode);
                    System.out.println("位置："+addressName);
                    map.put("附近地方",address);
                    map.put("所属区域",district);
                    map.put("所属地方code",areaCode);
                    map.put("位置",addressName);
                }
                insr.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
    }
}

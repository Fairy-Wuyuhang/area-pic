package com.wyh.areapic.service.impl;

import com.drew.imaging.jpeg.JpegMetadataReader;
import com.drew.imaging.jpeg.JpegProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.Tag;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Service
public class PictureServiceImpl {

  HashMap<String,Object>map=new HashMap<>();

  /**
   * 1.读取图片
   * @param file
   * @return
   */
  public Map<String,Object> readPic(File file) {
    System.out.println("开始读取图片信息...");
    Metadata metadata;
    try {
      metadata = JpegMetadataReader.readMetadata(file);
      Iterator<Directory> it = metadata.getDirectories().iterator();
      while (it.hasNext()) {
        Directory exif = it.next();
        Iterator<Tag> tags = exif.getTags().iterator();
        while (tags.hasNext()) {
          Tag tag = (Tag) tags.next();
          System.out.println(tag);
          if(tag.getTagName().equals("GPS Latitude")){
            String lat = pointToLatlong(tag.getDescription());
            map.put("x",lat);
            System.err.println("经度为："+lat);
          }else if(tag.getTagName().equals("GPS Longitude")){
            String log = pointToLatlong(tag.getDescription());
            map.put("y",log);
            System.err.println("纬度为："+log);
          }else if(tag.getTagName().equals("Date/Time")){
            System.err.println("拍摄的时间："+tag.getDescription());
            map.put("time",tag.getDescription());
          }
        }
      }
      System.out.println("图片信息读取完成！");
    } catch (JpegProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return map;
  }

  /**
   * 2.经纬度格式  转换
   * @param point 坐标点
   * @return
   */
  public static String pointToLatlong (String point ) {
    Double du = Double.parseDouble(point.substring(0, point.indexOf("°")).trim());
    Double fen = Double.parseDouble(point.substring(point.indexOf("°")+1, point.indexOf("'")).trim());
    Double miao = Double.parseDouble(point.substring(point.indexOf("'")+1, point.indexOf("\"")).trim());
    Double duStr = du + fen / 60 + miao / 60 / 60 ;
    return duStr.toString();
  }
}
